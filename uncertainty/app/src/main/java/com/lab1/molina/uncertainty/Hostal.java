package com.lab1.molina.uncertainty;

/**
 * Created by cooli on 21/03/2018.
 */

public class Hostal {
    private String name;
    private String address;
    private long phone;

    public Hostal(String name, String address, long phone){
        this.name=name;
        this.address=address;
        this.phone=phone;
    }

    public String getName(){
        return name;
    }

    public String getData(){
        String asList = "Direccion: "+address+"\nTelefono: (502) "+String.valueOf(phone);
        return asList;
    }
}
