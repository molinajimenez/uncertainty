package com.lab1.molina.uncertainty;

/**
 * Created by molin on 3/12/2018.
 */
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.nostra13.universalimageloader.core.ImageLoader.TAG;

public class listActT3 extends Fragment{
    public static final int PICK_IMAGE=100;
    Uri ImageURI;
    //ImageView imagenProfile;
    private FirebaseAuth mAuth;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        mAuth = FirebaseAuth.getInstance();
        final FirebaseUser currentUser = mAuth.getCurrentUser();
        final Context thiscontext=container.getContext();

        final View rootView = inflater.inflate(R.layout.tab3_list, container, false);
        Switch nameSwitch = (Switch)rootView.findViewById(R.id.switchName);

        final EditText txt = (EditText)rootView.findViewById(R.id.editTxt);
        final EditText txtPw = (EditText)rootView.findViewById(R.id.changePW);
        final EditText oldPw=(EditText)rootView.findViewById(R.id.oldPW);
        final EditText email=rootView.findViewById(R.id.actualEmail);

        /** montar imagen...
        Button changePic = (Button) rootView.findViewById(R.id.swapPic);
        changePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
            }
        });
        **/


        //* cambio de imagen y redondeado
        /**
        imagenProfile =(ImageView)rootView.findViewById(R.id.img);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.genericpic);
        RoundedBitmapDrawable rounded = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
        rounded.setCircular(true);
        imagenProfile.setImageDrawable(rounded);
         **/

        //* switch para el txtUser
        nameSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(!txt.isEnabled()){
                    String userID = mAuth.getCurrentUser().getUid();
                    DatabaseReference currentDB = FirebaseDatabase.getInstance().getReference().child("User").child(userID);
                    DatabaseReference usernameDB = currentDB.child("username");
                    usernameDB.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            txt.setText(dataSnapshot.getValue(String.class));
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.w(TAG, "onCancelled", databaseError.toException());
                        }
                    });
                    txt.setEnabled(true);
                } else{
                    txt.setEnabled(false);
                }

            }
        });

        //* switch para el txtPassword.
        Switch pwSwitch = (Switch)rootView.findViewById(R.id.switchPW);
        pwSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(!txtPw.isEnabled()) {
                    txtPw.setEnabled(true);
                    oldPw.setEnabled(true);
                    email.setEnabled(true);
                } else{
                    txtPw.setEnabled(false);
                    oldPw.setEnabled(false);
                    email.setEnabled(false);
                }
            }
        });

        //guardar cambios (consta que solo guarda los cambios de los txt si estos estan enabled)
        Button save=(Button) rootView.findViewById(R.id.button3);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt.isEnabled()) {
                    Query usernameQuery = FirebaseDatabase.getInstance().getReference().child("User").orderByChild("username").equalTo(txt.getText().toString().toLowerCase());
                    usernameQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getChildrenCount() > 0) {
                                Toast.makeText(thiscontext, "Username already taken. Choose a new username.", Toast.LENGTH_SHORT).show();
                            } else {
                                String userID = mAuth.getCurrentUser().getUid();
                                DatabaseReference currentDB = FirebaseDatabase.getInstance().getReference().child("User").child(userID);
                                DatabaseReference usernameDB = currentDB.child("username");
                                //Map newUser = new HashMap();
                                //newUser.put("username", txt.getText().toString());
                                usernameDB.setValue(txt.getText().toString());
                                //currentDB.setValue(newUser);
                                Toast.makeText(thiscontext, "Username updated.", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
                if(txtPw.isEnabled()){

                    AuthCredential credential = EmailAuthProvider
                            .getCredential(email.getText().toString().toLowerCase(), oldPw.getText().toString());

                    currentUser.reauthenticate(credential).addOnCompleteListener(
                            new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        if(txtPw.getText().toString().length()<6){
                                            Toast.makeText(thiscontext, "Password must be at least 6 characters long.",Toast.LENGTH_SHORT).show();
                                        }
                                        else {
                                            String newPassword = txtPw.getText().toString();

                                            currentUser.updatePassword(newPassword)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                Toast.makeText(thiscontext, "Password updated.", Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });
                                        }
                                    } else {
                                        Toast.makeText(thiscontext, "Authentication Failed", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                    );

                }


            }
        });



        return rootView;

    }
    private void openGallery(){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);

    }

    /**
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent dataImg){
        super.onActivityResult(requestCode,resultCode,dataImg);
        if(resultCode==RESULT_OK && requestCode==PICK_IMAGE){
            ImageURI=dataImg.getData();
            imagenProfile.setImageURI(ImageURI);

        }
    }
    */
}
