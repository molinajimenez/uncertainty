package com.lab1.molina.uncertainty;

import com.google.firebase.database.Exclude;

/**
 * Created by Mafer on 14/03/2018.
 */

public class Card {
    private String imgUrl;
    private String name;
    private String phone;
    private String mail;
    private String description;
    private String userUid;
    private String mKey;

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserUid(){ return userUid; }

    public Card(String imgUrl, String name, String phone, String mail, String description, String userUid) {
        this.imgUrl = imgUrl;
        this.name = name;
        this.phone = phone;
        this.mail = mail;
        this.description = description;
        this.userUid = userUid;

    }

    public Card(){}

    @Exclude
    public String getKey(){
        return mKey;
    }
    @Exclude
    public void setKey(String key){
        mKey = key;

    }




}
