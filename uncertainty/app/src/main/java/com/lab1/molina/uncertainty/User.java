package com.lab1.molina.uncertainty;

/**
 * Created by Mafer on 27/03/2018.
 */

public class User {
    private String user_id;
    private String email;
    private String phone_number;
    private String username;
    private String imageURL;

    public User(String user_id, String email, String phone_number, String username, String imageURL) {
        this.user_id = user_id;
        this.email = email;
        this.phone_number = phone_number;
        this.username = username;
        this.imageURL=imageURL;
    }

    public User() {
    }

    public void setImageURL(String imageURL){ this.imageURL=imageURL; }

    public String getImageURL(){ return imageURL; }

    @Override
    public String toString() {
        return "User{" +
                "user_id='" + user_id + '\'' +
                ", email='" + email + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", username='" + username + '\'' +
                '}';
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
