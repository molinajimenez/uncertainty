package com.lab1.molina.uncertainty;

/**
 * Created by Mafer on 31/03/2018.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;


public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {
    private Context mContext;
    private List<Card> mUploads;
    private OnItemClickListener mListener;

    public ImageAdapter(Context context, List<Card> uploads) {
        mContext = context;
        mUploads = uploads;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.linear_layout, parent, false);
        return new ImageViewHolder(v);
    }

    /**
     * method that creates the information that will be showned in the cardview
     * it retrievs the information from the databse
     * @param holder imageviewholder
     * @param position position of the item in the listview
     */
    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        Card uploadCurrent = mUploads.get(position);
        holder.textViewName.setText(uploadCurrent.getName() + "\n" + "Phone: " + uploadCurrent.getPhone() + " "
        + "Contact Email: " + uploadCurrent.getMail() +" " + "Description: " + uploadCurrent.getDescription());
        Picasso.with(mContext)
                .load(uploadCurrent.getImgUrl())
                .placeholder(R.mipmap.ic_launcher)
                .fit()
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return mUploads.size();
    }

    /**
     * finding and instanciando todas las views que vera el usuario en el feed
     */

    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener {
        public TextView textViewName;
        public ImageView imageView;

        public ImageViewHolder(View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.cardTitle);
            imageView = itemView.findViewById(R.id.cardImage);

            itemView.setOnClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    mListener.onItemClick(position);
                }
            }
        }


        /**
         * method that creates the options that the context menu will display
         * @param menu menu
         * @param v view
         * @param menuInfo menu info
         */
        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select Action");
            MenuItem doWhatever = menu.add(Menu.NONE, 1, 1, "Send form");
            MenuItem delete = menu.add(Menu.NONE, 2, 2, "Delete");
            MenuItem call = menu.add(Menu.NONE, 3, 3, "Call");

            doWhatever.setOnMenuItemClickListener(this);
            delete.setOnMenuItemClickListener(this);
            call.setOnMenuItemClickListener(this);
        }

        /**
         * method that indicates wich instructions execute in each option of the context menu
         * @param item menutiem selected
         * @return una instancia de un metodo
         */
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (mListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {

                    switch (item.getItemId()) {
                        case 1:
                            mListener.onWhatEverClick(position);
                            return true;
                        case 2:
                            mListener.onDeleteClick(position);
                            return true;
                        case 3:
                            mListener.onCallClick(position);
                            return true;
                    }
                }
            }
            return false;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);

        void onWhatEverClick(int position);

        void onDeleteClick(int position);

        void onCallClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }
}