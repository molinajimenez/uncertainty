package com.lab1.molina.uncertainty;

import android.*;
import android.Manifest;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.lab1.molina.uncertainty.listActT3.PICK_IMAGE;

/**
 * Created by cooli on 25/03/2018.
 */

public class NewCardView extends AppCompatActivity {

    private ImageView imageCardView;
    private Button changePic;
    private Uri ImageURI = null;
    private Button save;
    private Bitmap ImageBitmap;
    private EditText mail, phone, description, name;
    private StorageReference mStorageRef;
    private DatabaseReference myRef;
    private StorageTask mUploadTask;
    private static final int CAMERA_REQUEST_CODE = 5;
    private static final String TAG = "NewCardView";
    private Card newPost;
    public int storagePermission = 1;
    private Map<String, Card> cardMap = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mStorageRef = FirebaseStorage.getInstance().getReference("uploads");
        myRef = FirebaseDatabase.getInstance().getReference("uploads");
        setContentView(R.layout.new_cardview);
        changePic = findViewById(R.id.searchImage);
        save = findViewById(R.id.uploadCardview);

        Button openCamera = (Button) findViewById(R.id.btCamara);
        openCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(camera, CAMERA_REQUEST_CODE);

            }
        });
        changePic.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View view) {
                                             //here

                                             if(ContextCompat.checkSelfPermission(NewCardView.this,
                                                     android.Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {
                                                 Toast.makeText(NewCardView.this, "Success!", Toast.LENGTH_SHORT).show();
                                             } else{
                                                 requestStoragePermission();

                                             }
                                             openGallery();
                                         }
                                     }
        );

        imageCardView = (ImageView) findViewById(R.id.petImage);
        mail = findViewById(R.id.newEmail);
        phone = findViewById(R.id.phoneNumber);
        description = findViewById(R.id.petDescription);
        name = findViewById(R.id.petName);

        //doing the post
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isStringNull(mail.getText().toString().trim()) || isStringNull(phone.getText().toString().trim()) || isStringNull(name.getText().toString().trim()) || isStringNull(description.getText().toString().trim())){
                    Toast.makeText(NewCardView.this, "Please fill out all fields", Toast.LENGTH_SHORT).show();
                }
                else {
                    if(isEmailValid(mail.getText().toString().trim())) {
                        if (ImageURI == null) {
                            Toast.makeText(NewCardView.this, "Please upload an image", Toast.LENGTH_SHORT).show();
                        } else {
                            if (mUploadTask != null && mUploadTask.isInProgress()) {
                                Toast.makeText(NewCardView.this, "Upload in progress", Toast.LENGTH_SHORT).show();
                            } else {
                                uploadFile();
                                Intent intent = new Intent(NewCardView.this, introduction_Act.class);
                                startActivity(intent);
                                ImageURI = null;
                            }
                        }
                    }
                    else{
                        Toast.makeText(NewCardView.this, "Please upload a valid email", Toast.LENGTH_SHORT).show();
                    }
                }


            }

        });
    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

            new AlertDialog.Builder(this)
                    .setTitle("Permission needed")
                    .setMessage("Permssion needed to upload image")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(NewCardView.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, storagePermission);

                        }

                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .create().show();

        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, storagePermission);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==storagePermission) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show();
            } else{
                Toast.makeText(this, "permission NOT granted", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);

    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * method to put the image on the layout
     * @param requestCode
     * @param resultCode
     * @param dataImg
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent dataImg) {
        super.onActivityResult(requestCode, resultCode, dataImg);
        if (requestCode == CAMERA_REQUEST_CODE) {
            Log.d(TAG, "onActivityResult: done taking a photo");
            Log.d(TAG, "onActivityResult: intenting to navigate to final share screen");
            ImageBitmap  = (Bitmap) dataImg.getExtras().get("data");
            ImageURI = dataImg.getData();
            imageCardView.setImageBitmap(ImageBitmap);


            //returning to this screen i guess for publishing the photo
        }
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            ImageURI = dataImg.getData();
            imageCardView.setImageURI(ImageURI);
            Picasso.with(this).load(ImageURI).into(imageCardView);


        }
    }

    private boolean isStringNull(String string) {
        Log.d(TAG, "isStringNull: checking if null.");
        if (string.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    private String getFileExtension(Uri uri){
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadFile(){

            if (ImageURI != null) {
                StorageReference fileReference = mStorageRef.child(System.currentTimeMillis()
                        + "." + getFileExtension(ImageURI));
                mUploadTask = fileReference.putFile(ImageURI).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(NewCardView.this, "Upload successful", Toast.LENGTH_LONG).show();
                        Card upload = new Card(taskSnapshot.getDownloadUrl().toString(), name.getText().toString().trim(), phone.getText().toString().trim(),
                                mail.getText().toString().trim(), description.getText().toString().trim(), FirebaseAuth.getInstance().getCurrentUser().getUid());
                        String uploadId = myRef.push().getKey();
                        myRef.child(uploadId).setValue(upload);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(NewCardView.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                    }
                });

            } else {
                Toast.makeText(this, "No image selected", Toast.LENGTH_SHORT);
            }

    }



}

