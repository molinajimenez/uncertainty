package com.lab1.molina.uncertainty;

/**
 * Created by Francisco Molina on 3/12/2018.
 */

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

public class calcActT2 extends Fragment {
    View rootView;
    private Hostal[] hostales;
    private String[] groups;
    private String[][] children;
    GridLayout mainGrid;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        hostales = new Hostal[]{new Hostal("Palvet", "18 Ave. 19-39, zona 10", 23682481), new Hostal("Albergue Municipal de Mascotas", "16 Ave. 11-54, zona 11", 24488853)};


        groups = new String[]{hostales[0].getName(),hostales[1].getName()};

        children = new String[][]{
                {hostales[0].getData()},
                {hostales[1].getData()}
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tab2_calc, container, false);
        return rootView;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mainGrid=(GridLayout)rootView.findViewById(R.id.mainGrid);


        //Evento
        setSingleEvent(mainGrid);


    }

    private void setSingleEvent(GridLayout mainGrid) {

        for(int i=0; i<mainGrid.getChildCount();i++){
            CardView objetosHijos= (CardView)mainGrid.getChildAt(i);
            final int finalI = i;
            objetosHijos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(rootView.getContext(), "Usted hizo click en el elemento " + finalI, Toast.LENGTH_SHORT).show();
                }
            });

        }
    }


}
