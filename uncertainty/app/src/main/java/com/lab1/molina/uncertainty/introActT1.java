package com.lab1.molina.uncertainty;

/**
 * Created by Francisco Molina on 3/12/2018.
 */

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class introActT1 extends Fragment implements ImageAdapter.OnItemClickListener {
    private ImageAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private DatabaseReference myRef;
    private List<Card> list;
    private FirebaseStorage mStorage;
    private ValueEventListener mDBListener;
    private ProgressBar mProgressCircle;
    public String selectedMail = " ";
    private static final int REQUEST_CALL = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.tab1_intro, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mProgressCircle = rootView.findViewById(R.id.progress_circle);

        list = new ArrayList<>();
        mAdapter = new ImageAdapter(getActivity(), list);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(introActT1.this);

        //firebase stuff
        mStorage = FirebaseStorage.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference("uploads");

        mDBListener = myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Card upload = postSnapshot.getValue(Card.class);
                    upload.setKey(postSnapshot.getKey());
                    list.add(upload);
                }

                mAdapter.notifyDataSetChanged();
                mProgressCircle.setVisibility(View.GONE);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        return rootView;

    }

    @Override
    public void onItemClick(int position) {
        Toast.makeText(getActivity(), "Loading..", Toast.LENGTH_SHORT).show();

    }

    /**
     * method that obtains the email from the post selected, and transfer it to the activity
     * where the adopting form will be send
     * @param position of the cardview (post) selected
     */
    @Override
    public void onWhatEverClick(int position) {
        Card selectedItem = list.get(position);
        selectedMail = selectedItem.getMail();
        Toast.makeText(getActivity(), "Loading..", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity(), FormularioActivity.class);
        intent.putExtra("Email", selectedMail);
        startActivity(intent);


    }

    /**
     * method to delete the post made by the user
     * a user can only delete the posts that he made
     * @param position of the cardview (post) selected or position in the listview
     */

    @Override
    public void onDeleteClick(int position) {
        Card selectedItem = list.get(position);

        //checks if the user Uid of the one login is the same as the one that the card has saved on
        //firebase data
        if(selectedItem.getUserUid().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
            //if equals the user can delete the post

            final String selectedKey = selectedItem.getKey();

            //getting the imageUrl from the reference storage with the help of the cardview obtain
            //with the position of the listview
            StorageReference imageRef = mStorage.getReferenceFromUrl(selectedItem.getImgUrl());
            imageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    myRef.child(selectedKey).removeValue();
                    Toast.makeText(getActivity(), "Item deleted", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else{
            //if the Uid is not equals a warning message appears
            Toast.makeText(getActivity(), "You did not post this item, hence you cannot delete it.", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * method to make the phone call once the permission was accepted by the user
     * @param phone number
     */

    public void makePhone(String phone){
        if (phone.trim().length() > 0) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL );
            } else {
                String dial = "tel:" + phone;
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }

        } else {
            Toast.makeText(getActivity(), "Enter Phone Number", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * method that executes when the user click the third option of the context menu
     * calls the makephone() method and obtains the phone number of the post selected
     * @param position of the post in the listview
     */

    public void onCallClick(int position){
        Card selectedItem = list.get(position);
        String phone = selectedItem.getPhone();
        makePhone(phone);

    }

    /**
     * asks permission to the user to do the phone call
     * @param requestCode a code
     * @param permissions permissions on the manifest
     * @param grantResults results
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CALL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
               makePhone("123");
            } else {
                Toast.makeText(getActivity(), "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        myRef.removeEventListener(mDBListener);
    }
}
