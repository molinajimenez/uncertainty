package com.lab1.molina.uncertainty;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;

/**
 * Created by Mafer on 27/03/2018.
 */

public class FirebaseMethods {
    private static final String TAG = "FirebaseMethods" ;


    /**
     * method to check if a username already exists on the database
     * it compares the default username given to the user cause when a user registers the app creates
     * a default username for them (similar to instragram)
     * the default username is "name.firstname"
     * @param username of the user registering 'now'
     * @param dataSnapshot the snapshot of the databse with all the childs
     * @return true if the usernames are equals and false if usernames ar unequals
     */
    public boolean checkIfUsernameExists(String username, DataSnapshot dataSnapshot){
        Log.d(TAG, "checkIfUsernameExists: checking if" + username + "already exists");
        User user = new User();
        //loop to iterate the nodes created on the snapshot in firebase
        for(DataSnapshot ds: dataSnapshot.getChildren()){
            user.setUsername(ds.getValue(User.class).getUsername());

            //we compare all the usernames in the database with the @param 'username'
            if(StringManipulation.expandUsername(user.getUsername()).equals(username)){
                return true;

            }


        }
        return false;
    }
}
