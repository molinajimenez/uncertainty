package com.lab1.molina.uncertainty;

import android.Manifest;

/**
 * Created by Mafer on 28/03/2018.
 */

public class Permissions {

    /**
     * method to ask permission to open the camera
     */
    public static final String[] CAMERA_PERMISSION = {
            Manifest.permission.CAMERA
    };


    /**
     * method to ask permission to write
     */
    public static final String[] WRITE_STORAGE_PERMISSION = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    /**
     * method to ask permission to open the read
     */
    public static final String[] READ_STORAGE_PERMISSION = {
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    /**
     * method to ask permission make a phone call
     */
    public static final String[] CALL_PHONE_PERMISSION = {
            Manifest.permission.CALL_PHONE
    };

}