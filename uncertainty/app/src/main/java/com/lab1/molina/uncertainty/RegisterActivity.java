package com.lab1.molina.uncertainty;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements  View.OnClickListener{
    private static final String TAG = "abc" ;
    private EditText mail;
    private EditText pass;
    private EditText name;
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private DatabaseReference myRef;
    private FirebaseMethods fbMethods;
    private EditText usernamee;
    private String append = "";

    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_register);
            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance();
            myRef = mDatabase.getReference();
            fbMethods = new FirebaseMethods();
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            CardView logbt = (CardView) findViewById(R.id.cardView);
            mail = (EditText) findViewById(R.id.email);
            pass = (EditText) findViewById(R.id.password);
            usernamee = (EditText) findViewById(R.id.editText);
            logbt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   createAccount();

                }
            });
        }


    /**
     * method to check if some information is empty
     * @param string some text
     * @return true if its empty and false if not
     */
    private boolean isStringNull(String string){
        Log.d(TAG, "isStringNull: checking if null.");
        if(string.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * method to put the '@' on th keyboard each time the user enters the email
     * @param email email
     * @return a matcher
     */

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * method that creates a new account with the user data in firebase
     * method extract from the documentation on firebase.com
     */
    private void createAccount(){
            final String email = mail.getText().toString().trim();
            final String password = pass.getText().toString().trim();
            final String username = usernamee.getText().toString().trim();
            if(isStringNull(email) || isStringNull(password) || isStringNull(username)){
                Toast.makeText(RegisterActivity.this, "Please fill out all the fields.", Toast.LENGTH_SHORT).show();
            }
            else {
                if(isEmailValid(email)) {
                    Query usernameQuery = FirebaseDatabase.getInstance().getReference().child("User").orderByChild("username").equalTo(username);
                    usernameQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getChildrenCount() > 0) {
                                Toast.makeText(RegisterActivity.this, "Username already taken. Choose a new username.", Toast.LENGTH_SHORT).show();
                            } else {
                                mAuth.createUserWithEmailAndPassword(email, password)
                                        .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                            @Override
                                            public void onComplete(@NonNull Task<AuthResult> task) {
                                                if (task.isSuccessful()) {
                                                    // Sign in success, update UI with the signed-in user's information
                                                    Log.d(TAG, "createUserWithEmail:success");

                                                    String userID = mAuth.getCurrentUser().getUid();
                                                    DatabaseReference currentDB = FirebaseDatabase.getInstance().getReference().child("User").child(userID);
                                                    Map newUser = new HashMap();
                                                    newUser.put("username", username);
                                                    newUser.put("user_id", userID);
                                                    //newUser.put("profilepic",R.drawable.genericpic);
                                                    currentDB.setValue(newUser);

                                                    /** final FirebaseUser user = mAuth.getCurrentUser();
                                                     myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override public void onDataChange(DataSnapshot dataSnapshot) {
                                                    //checking if the username is not in use
                                                    if(fbMethods.checkIfUsernameExists(username, dataSnapshot)){
                                                    append = myRef.push().getKey().substring(3,10);
                                                    Log.d(TAG, "onDataChange: username already exists. Appending random string to name: " + append);
                                                    }



                                                    }

                                                    @Override public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                    });*/
                                                    Intent intent = new Intent(RegisterActivity.this, introduction_Act.class);
                                                    startActivity(intent);
                                                    //updateUI(user);
                                                } else {
                                                    // If sign in fails, display a message to the user.
                                                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                                    Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                                            Toast.LENGTH_SHORT).show();
                                                    //updateUI(null);
                                                }


                                                // ...
                                            }
                                        });

                            }

                        }


                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
                else{
                    Toast.makeText(RegisterActivity.this, "Please upload a valid email", Toast.LENGTH_SHORT).show();
                }
            }


        }

    @Override
    public void onClick(View view) {
            createAccount();
    }
}


