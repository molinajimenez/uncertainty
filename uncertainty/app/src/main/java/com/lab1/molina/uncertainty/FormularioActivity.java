package com.lab1.molina.uncertainty;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormularioActivity extends AppCompatActivity {

    EditText mailForm,phoneForm,ageForm,nameForm,dpiForm;
    TextView formRecipient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);
        mailForm=findViewById(R.id.emailAdr);
        phoneForm=findViewById(R.id.phoneNumber);
        ageForm=findViewById(R.id.age);
        nameForm=findViewById(R.id.adoptantName);
        dpiForm=findViewById(R.id.idNumber);
        formRecipient=findViewById(R.id.mailRecipient);



            Intent in = this.getIntent();
            String correo = in.getStringExtra("Email");
            formRecipient.setText(correo);

            Toast.makeText(this, correo, Toast.LENGTH_SHORT).show();

            Button sendForm = findViewById(R.id.send_formu);
            sendForm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isStringNull(mailForm.getText().toString()) || isStringNull(phoneForm.getText().toString()) || isStringNull(ageForm.getText().toString()) || isStringNull(nameForm.getText().toString()) || isStringNull(dpiForm.getText().toString())){
                        Toast.makeText(FormularioActivity.this, "You must fill out the fields", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if(isEmailValid(mailForm.getText().toString()))
                        {
                            String to = formRecipient.getText().toString();
                            String subject = "Adoption Form";
                            String message = "Mail:" + mailForm.getText().toString() + "\nPhone:" + phoneForm.getText().toString() + "\nAge:" + ageForm.getText().toString() + "\nName:" + nameForm.getText().toString() + "\nDPI:" + dpiForm.getText().toString();

                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
                            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                            intent.putExtra(Intent.EXTRA_TEXT, message);

                            intent.setType("message/rfc822");

                            startActivity(Intent.createChooser(intent, "Select Email App"));
                        }
                        else{
                            Toast.makeText(FormularioActivity.this, "Please upload a valid email", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isStringNull(String string){
        if(string.equals("")) {
            return true;
        } else {
            return false;
        }

    }
}
