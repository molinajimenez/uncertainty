package com.lab1.molina.uncertainty;

/**
 * Created by Mafer on 27/03/2018.
 */

public class StringManipulation {

    //yo se que static no es buena practica pero lo use solo porque se me iba a hacer mas facil el
    //uso de este metodo
    public static String expandUsername(String username){
        return username.replace(".", " ");
    }

    public static String condenseUsername(String username){
        return username.replace(" ", ".");

    }
}
